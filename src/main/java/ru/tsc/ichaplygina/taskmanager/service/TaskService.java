package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    public List<Task> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    public Task findById(@NotNull final String userId, @NotNull final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void remove(@NotNull final String userId, List<Task> tasks) {
        for (Task task : tasks) task.setUserId(userId);
        repository.deleteAll(tasks
                .stream()
                .filter(t -> userId.equals(t.getUserId()))
                .collect(Collectors.toList())
        );
    }

    @Transactional
    public void removeAll(@NotNull final String userId) {
        repository.deleteAllByUserId(userId);
    }

    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Nullable
    @Transactional
    public Task write(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return repository.save(task);
    }

    @NotNull
    @Transactional
    public List<Task> write(@NotNull final String userId, @NotNull final List<Task> tasks) {
        for (Task task : tasks) task.setUserId(userId);
        return repository.saveAll(tasks);
    }

}
