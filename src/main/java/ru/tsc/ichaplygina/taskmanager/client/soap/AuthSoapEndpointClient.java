package ru.tsc.ichaplygina.taskmanager.client.soap;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthSoapEndpointClient {

    public static AuthEndpoint getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/AuthEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "AuthEndpointImplService";
        @NotNull final String ns = "http://endpoint.taskmanager.ichaplygina.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final AuthEndpoint result = Service.create(url, name).getPort(AuthEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
