package ru.tsc.ichaplygina.taskmanager.exception;

import org.jetbrains.annotations.NotNull;

public final class AccessDeniedException extends RuntimeException {

    @NotNull
    private static final String MESSAGE = "Access denied!";

    public AccessDeniedException() {
        super(MESSAGE);
    }

}
