package ru.tsc.ichaplygina.taskmanager.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

public class TasksRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    final Task task1 = new Task("Test Task 1");

    @NotNull
    final Task task2 = new Task("Test Task 2");

    @NotNull
    final Task task3 = new Task("Test Task 3");

    @NotNull
    final Task task4 = new Task("Test Task 4");

    @NotNull
    final Task task5 = new Task("Test Task 5");

    @NotNull
    final Task task6 = new Task("Test Task 6");

    @NotNull
    final List<Task> taskList1 = Arrays.asList(task1, task2, task3);

    @NotNull
    final List<Task> taskList2 = Arrays.asList(task4, task5, task6);

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<Task> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Task.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskList1, header));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskList2, header));
        @NotNull final String findUrl = TASK_URL + "findById/" + task6.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(3, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        @NotNull final String url = BASE_URL + "remove/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskList1, header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeAll() {
        @NotNull final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final String url = BASE_URL + "save/";
        taskList1.get(0).setName("Changed Name");
        sendRequestList(url, HttpMethod.PUT, new HttpEntity<>(taskList1, header)).getBody();
        @NotNull final String findUrl = TASK_URL + "findById/" + task1.getId();
        Assert.assertEquals("Changed Name", sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().getName());
    }

}
