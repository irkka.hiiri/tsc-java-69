package ru.tsc.ichaplygina.taskmanager.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

public class TaskRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/task/";

    @NotNull
    private static final String TASKS_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final Task task1 = new Task("Test Task 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2");

    @NotNull
    private final Task task3= new Task("Test Task 3");

    @NotNull
    private final Task task4 = new Task("Test Task 4");

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<Task> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Task.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task3, header));
    }

    @After
    public void clean() {
        @NotNull final String url = TASKS_URL + "removeAll/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String expected = task4.getName();
        @NotNull final String url = BASE_URL + "add/";
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(task4, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task task = response.getBody();
        Assert.assertNotNull(task);
        @NotNull final String actual = task.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String expected = task1.getId();
        @NotNull final String url = BASE_URL + "findById/" + expected;
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task task = response.getBody();
        Assert.assertNotNull(task);
        final String actual = task.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = BASE_URL + "removeById/" + id;
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        @NotNull final String urlFind = BASE_URL + "findById/" + id;
        Assert.assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final String url = BASE_URL + "save/";
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.PUT, new HttpEntity<>(task4, header));
        @NotNull final String urlFind = BASE_URL + "findById/";
        Assert.assertNull(sendRequest(urlFind + task4.getId(), HttpMethod.GET, new HttpEntity<>(header)).getBody());
        @NotNull final String expected = "Test Task One";
        task1.setName(expected);
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(task1, header));
        @NotNull final String actual = sendRequest(urlFind + task1.getId(), HttpMethod.GET, new HttpEntity<>(header)).getBody().getName();
        Assert.assertEquals(expected, actual);
    }

}
