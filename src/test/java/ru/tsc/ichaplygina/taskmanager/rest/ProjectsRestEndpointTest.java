package ru.tsc.ichaplygina.taskmanager.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

public class ProjectsRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project/";

    @NotNull
    final Project project1 = new Project("Test Project 1");

    @NotNull
    final Project project2 = new Project("Test Project 2");

    @NotNull
    final Project project3 = new Project("Test Project 3");

    @NotNull
    final Project project4 = new Project("Test Project 4");

    @NotNull
    final Project project5 = new Project("Test Project 5");

    @NotNull
    final Project project6 = new Project("Test Project 6");

    @NotNull
    final List<Project> projectList1 = Arrays.asList(project1, project2, project3);

    @NotNull
    final List<Project> projectList2 = Arrays.asList(project4, project5, project6);

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<Project> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Project.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(projectList1, header));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(projectList2, header));
        @NotNull final String findUrl = PROJECT_URL + "findById/" + project6.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(3, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        @NotNull final String url = BASE_URL + "remove/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(projectList1, header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeAll() {
        @NotNull final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final String url = BASE_URL + "save/";
        projectList1.get(0).setName("Changed Name");
        sendRequestList(url, HttpMethod.PUT, new HttpEntity<>(projectList1, header)).getBody();
        @NotNull final String findUrl = PROJECT_URL + "findById/" + project1.getId();
        Assert.assertEquals("Changed Name", sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().getName());
    }

}
