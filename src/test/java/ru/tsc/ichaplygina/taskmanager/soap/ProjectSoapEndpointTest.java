package ru.tsc.ichaplygina.taskmanager.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.ProjectEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.ProjectsEndpoint;
import ru.tsc.ichaplygina.taskmanager.client.soap.AuthSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.ProjectSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.ProjectsSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class ProjectSoapEndpointTest {

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static ProjectEndpoint projectEndpoint;

    @NotNull
    private static ProjectsEndpoint projectsEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final Project project1 = new Project("Test Project 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2");

    @NotNull
    private final Project project3 = new Project("Test Project 3");

    @NotNull
    private final Project project4 = new Project("Test Project 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("user", "user").isSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        projectsEndpoint = ProjectsSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull final BindingProvider projectsBindingProvider = (BindingProvider) projectsEndpoint;
        Map<String, List<String>> headers= CastUtils.cast((Map)authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        projectsBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        projectEndpoint.add(project1);
        projectEndpoint.add(project2);
        projectEndpoint.add(project3);
    }

    @After
    @SneakyThrows
    public void clean() {
        projectsEndpoint.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String expected = project4.getName();
        @Nullable final Project project = projectEndpoint.add(project4);
        Assert.assertNotNull(project);
        @NotNull final String actual = project.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String expected = project1.getId();
        @Nullable final Project project = projectEndpoint.findById(expected);
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        @NotNull final String id = project1.getId();
        projectEndpoint.removeById(id);
        Assert.assertNull(projectEndpoint.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        projectEndpoint.save(project4);
        Assert.assertNull(projectEndpoint.findById(project4.getId()));
        @NotNull final String expected = "Test Project One";
        project1.setName(expected);
        projectEndpoint.save(project1);
        @NotNull final String actual = projectEndpoint.findById(project1.getId()).getName();
        Assert.assertEquals(expected, actual);
    }

}
