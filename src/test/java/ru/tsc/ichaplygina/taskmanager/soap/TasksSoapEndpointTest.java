package ru.tsc.ichaplygina.taskmanager.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.TaskEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.TasksEndpoint;
import ru.tsc.ichaplygina.taskmanager.client.rest.TaskRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.rest.TasksRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.AuthSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.TaskSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.TasksSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.HttpCookie;
import java.util.*;

public class TasksSoapEndpointTest {

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static TasksEndpoint tasksEndpoint;

    @NotNull
    private static TaskEndpoint taskEndpoint;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    final Task task1 = new Task("Test Task 1");

    @NotNull
    final Task task2 = new Task("Test Task 2");

    @NotNull
    final Task task3 = new Task("Test Task 3");

    @NotNull
    final Task task4 = new Task("Test Task 4");

    @NotNull
    final Task task5 = new Task("Test Task 5");

    @NotNull
    final Task task6 = new Task("Test Task 6");

    @NotNull
    final List<Task> taskList1 = Arrays.asList(task1, task2, task3);

    @NotNull
    final List<Task> taskList2 = Arrays.asList(task4, task5, task6);

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("user", "user").isSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        tasksEndpoint = TasksSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull final BindingProvider tasksBindingProvider = (BindingProvider) tasksEndpoint;
        Map<String, List<String>> headers= CastUtils.cast((Map)authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        tasksBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void initTest() {
        tasksEndpoint.add(taskList1);
    }

    @After
    public void clean() {
        tasksEndpoint.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        tasksEndpoint.add(taskList2);
        Assert.assertNotNull(taskEndpoint.findById(task6.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @Nullable final List<Task> tasks = tasksEndpoint.findAll();
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        tasksEndpoint.remove(taskList1);
        Assert.assertNull(tasksEndpoint.findAll());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeAll() {
        tasksEndpoint.removeAll();
        Assert.assertNull(tasksEndpoint.findAll());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        taskList1.get(0).setName("Changed Name");
        @NotNull final List<Task> tasks = tasksEndpoint.save(taskList1);
        Assert.assertEquals("Changed Name", tasks.get(0).getName());
    }

}
