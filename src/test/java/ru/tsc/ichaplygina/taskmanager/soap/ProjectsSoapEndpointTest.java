package ru.tsc.ichaplygina.taskmanager.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.ProjectEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.ProjectsEndpoint;
import ru.tsc.ichaplygina.taskmanager.client.rest.ProjectRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.rest.ProjectsRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.AuthSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.ProjectSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.ProjectsSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.HttpCookie;
import java.util.*;

public class ProjectsSoapEndpointTest {

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static ProjectsEndpoint projectsEndpoint;

    @NotNull
    private static ProjectEndpoint projectEndpoint;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    final Project project1 = new Project("Test Project 1");

    @NotNull
    final Project project2 = new Project("Test Project 2");

    @NotNull
    final Project project3 = new Project("Test Project 3");

    @NotNull
    final Project project4 = new Project("Test Project 4");

    @NotNull
    final Project project5 = new Project("Test Project 5");

    @NotNull
    final Project project6 = new Project("Test Project 6");

    @NotNull
    final List<Project> projectList1 = Arrays.asList(project1, project2, project3);

    @NotNull
    final List<Project> projectList2 = Arrays.asList(project4, project5, project6);

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("user", "user").isSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        projectsEndpoint = ProjectsSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull final BindingProvider projectsBindingProvider = (BindingProvider) projectsEndpoint;
        Map<String, List<String>> headers= CastUtils.cast((Map)authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        projectsBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void initTest() {
        projectsEndpoint.add(projectList1);
    }

    @After
    public void clean() {
        projectsEndpoint.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        projectsEndpoint.add(projectList2);
        Assert.assertNotNull(projectEndpoint.findById(project6.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @Nullable final List<Project> projects = projectsEndpoint.findAll();
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        projectsEndpoint.remove(projectList1);
        Assert.assertNull(projectsEndpoint.findAll());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeAll() {
        projectsEndpoint.removeAll();
        Assert.assertNull(projectsEndpoint.findAll());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        projectList1.get(0).setName("Changed Name");
        @NotNull final List<Project> projects = projectsEndpoint.save(projectList1);
        Assert.assertEquals("Changed Name", projects.get(0).getName());
    }

}
